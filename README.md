# hugo-neon-dreams

A hugo theme based off of the popular VSCode synthwave-x-fluoromachine theme. 


## Quick start 

- 
## Features
- index as a start page
- it glows

## Example Config.toml

```toml
baseurl = "/"
title = "Dylan Storey"
paginate = "5" # Number of posts per page
theme = "neon-dreams"
googleAnalytics = "" # Enable Google Analytics by entering your tracking id
summaryLength = 20



[Params]
  description = "Dumping ground for shitty thoughts." # Site description. Used in meta description
  copyright = "Dylan Storey" # Footer copyright holder, otherwise will use site title
  opengraph = true # Enable OpenGraph if true
  schema = true # Enable Schema
  twitter_cards = true # Enable Twitter Cards if true
  readmore = true # Show "Read more" button in list if true
  toc = true # Enable Table of Contents
  pager = true # Show pager navigation (prev/next links) at the bottom of pages if true
  dateformat = "2006-01-02" # Change the format of dates
  
  landing_icons = [
            { name = "About Me",  url = "about",    icon = "fas fa-address-card" },            
            { name =  "Projects", url = "projects", icon = "fas fa-project-diagram" },
            { name = "Blog",      url = "blog",     icon = "fas fa-journal-whills" },           
            { name ="Recipes",    url = "recipe",   icon = "fas fa-bacon" },        
            ]

[Params.logo]
  emoji = "(◣_◢)"
  title = " " # Logo title, otherwise will use site title
  subtitle = " " # Logo subtitle

[Params.sidebar]
  supress = ["about"]

[Params.widgets]
 default = ["recent","categories","taglist"]
 blog = ["recent","categories","taglist"]
 recipe = ["categories","taglist"]
 recent_num = 5 # Set the number of articles in the "Recent articles" widget

[Params.meta]
  default = ["date","tags"]

```


## Configuration Documentation


### Landing Page Icons

```toml
landing_icons = [
          { name = "About Me",  url = "about",    icon = "fas fa-address-card" },            
          { name =  "Projects", url = "projects", icon = "fas fa-project-diagram" },
          { name = "Blog",      url = "blog",     icon = "fas fa-journal-whills" },           
          { name ="Recipes",    url = "recipe",   icon = "fas fa-bacon" },        
          ]
```
This generates the [bootstrap cards](https://getbootstrap.com/docs/4.0/components/card/) for the landing page. 

This is somewhat required as a section. 
- The name is the text displayed on the screen
- The url is the what the link points to
- The icon is the [Font Awesome](https://fontawesome.com/) icon to use 

### Logo 

```toml
[Params.logo]
  emoji = "(◣_◢)"
  title = " " # Logo title, otherwise will use site title
  subtitle = " " # Logo subtitle
```

This is the Logo for the site. It is intended to be an emoji, you can include a title and/or subtitle to go with it. 


### Sidebar Supression

```toml
[Params.sidebar]
  supress = ["about"]
```
Simply provide sections that should supress the sidebar where widgets are rendered normally. 


### Widget Configuration


```toml
[Params.widgets]
 default = ["recent","categories","taglist"]
 blog = ["recent","categories","taglist"]
 recipe = ["categories","taglist"]
 recent_num = 5 # Set the number of articles in the "Recent articles" widget
 ```

 Configure the widgets for a given section. Also setting the global recent_num for the `recent` widget. 


 ### Meta Configuration

 ```toml
 [Params.meta]
  default = ["date","tags"]
  ```

  Configure the meta-data presented in the default summary of a section. 