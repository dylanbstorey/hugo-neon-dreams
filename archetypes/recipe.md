---
draft: true
tags: 
  - ""
categories:
  - ""
layout: recipe
type: recipe
Title: "{{ replace .Name "-" " " | title }}"
ingredients:
    - [amount , ingredient]
macros: 
    - [amount , type]
serving_size: ""
servings : X 
prep_time : X min
cook_time : X min
description : "Short recipe description here."
---

** Cooking Instructions Go Here**
