---
draft: false
tags: 
  - "Simple"
categories:
  - "Breakfast"
layout: recipe
type: recipe
Title: "Beta Bread"
ingredients:
    - [1 cup , water]
macros: 
    - [amount , type]
serving_size: ".5 Cup"
servings : 2 
prep_time : 1 min
cook_time : 15 min
description : "How to Boild Water."
---

1. Get a pot
2. Pour 1 cup of cold water in it.
3. Place over high heat on stove.
4. Watch it. 